# Desarrollo de modelo de pronóstico de ventas para una bebida, a partir de un datos históricos, como resolución a la prueba DS ArcoPrime.

## 1. Dependencias

- python 3.8.10
- dependencias en `setup/requirements.txt`

## 2. Configuration

### 2.1. python 3.8.10

Python3.8.10 es posible instalarlo desde link del website oficial:
(https://www.python.org/downloads/release/python-3810/)

#### 2.1.1. Instalación directa Linux
```console
sudo apt install python3.8.10
```
### 2.2  Environment del proyecto
```console
mkdir env
python3 -m venv env
source env/bin/activate (linux) source env/Scripts/activate (windows)
```
### 2.3. Librerias

Para instalar librerías se utiliza el gestor `pip==22.1.2`, por lo que se pueden instalar mediante el comando:
```console
pip install --upgrade pip
pip install -r setup/requirements.txt
```
En entorno windows es posible que para instalar la libreria statsmodels se requiera actualizar la version de Visual C++ >= 14.0. Puede descargarse e instalarse utilizando las herramientas de VisualStudioBuildTools: https://visualstudio.microsoft.com/es/downloads/

## 3. Agregar el kernel sales al listado de Jupyter notebook
```console
python3 -m ipykernel install --user --name sales --display-name sales
```

## 4. Verificar que kernel riego fue agregado a Jupyter notebook
Deberia aparecer riego en el listado
```console
jupyter kernelspec list
```

## 5. Run Sales-Model Training Job

Ejecutar notebooks en directorio "/training" del proyecto:
01-DataExploration.ipynb
02-ModelBuilder.ipynb

Archivo de pesos del modelo con el mejor performance obtenido en PyCaret: training/data/models/sales_best.pkl

## 6. Respuesta al test

Test_Answer.pdf

## 7. TODO 

Levantar servicio de inferencias para el predictor de ventas en directorio "/service" a partir del archivo de pesos obtenido, utilizando Docker -app.py y disponibilizarlo en localhost:8001







