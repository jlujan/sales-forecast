# import the regression module
from pycaret.regression import *
import matplotlib.pyplot as plt

class PycaretModels():
    """This class intance build PyCaret Module to train, compare and evaluate classical supervised trainig
    algorithms to predict daily sales of our dataset"""
    def Compare(df, test, target_feature):
        """Train the regressors set and evaluate their performance against the validation dataset
        
    --------------
    Parameters:
    -Train dataset: pd.DataFrame
    -Validation dataset: pd.DataFrame
    target_feature: column to predict -> str

    Output:
    - Best Model Weights based on MAPE metrics
    -"""
        print("Comparing Models")
        # initialize setup
        s = setup(data = df, 
                  test_data = test,
                  target = target_feature, 
                  fold_strategy = 'timeseries', 
                  numeric_features =['precio_ref','unidades_total'], 
                  fold = 3, transform_target = True,
                  session_id = 123)
        best = compare_models(sort = 'MAPE')
        return best
    
    def PerformancePlot(best_model):
        """Plot Best Model Performance on Validation Dataset
        -----
        Parameters:
        -best_model: Best Model Weights
        """
        print("Best Model Performance")
        evaluate_model(best_model)
        return 

    def ResidualsPlot(best_model):
        """Plot Best Model Residuals and R2 on Validation Dataset
        -----
        Parameters:
        -best_model: Best Model Weights
        """
        print("Best Model Residuals")
        plot_model(best_model, plot = 'residuals')
        return

    def TestDataForecast(testDS, best_model):
        """Run model prediction against testing data
        -----
        Parameters:
        -testDS: Testing dataset: pd.DataFrame
        -best_model: Best Model Weights

        Output:
        predictions: pd.Series with predicted sales values
        """
        print("BestModel PErformance on Test Dataset 2022-Feb")
        predictions = predict_model(best_model, data=testDS)
        return predictions

    def SaveWeights(best_model):
        """Save weights in to .pkl file format
        ---------
        Parameters:
        -best_model: Best Model Weights
        """
        save_model(best_model, 'data/models/sales_best')

