import os
import numpy as np
import pandas as pd


columns = ['monto_total', 'precio_ref', 'unidades_total',	'year',	'month', 'day',	'dayofweek','workingday']
class Split_Dataset():

    def StdFormat(df_path, paths):
        """Split train dataset in conventional format:
        - Training = 70% of the data
        - Validation = 30% of the data
        - Randomize the datset

        Parameters:
        - df: train_df -> pd.DataFrame
        """
        # 1. Create target column shifting + 1day total sales from "monto_total"
        df = pd.read_csv(df_path)
        shift_interval = 1 # 1 day step in future
        df['target'] = df['monto_total'].shift(-shift_interval)
        df = df[:-1] # remove last row with NaN target value
        np.random.seed(5)
        l = list(df.index)
        np.random.shuffle(l)
        df = df.loc[l]
        rows = df.shape[0]
        train = int(.7 * rows)
        test = rows-train
        rows, train, test

        # Write Training Set
        TrainData = df.iloc[:train].to_csv(os.path.join(paths.data,'train.csv')
                                ,index=False,header=False
                                ,columns=columns)
        # Write Validation Set
        ValidationData = df.iloc[train:].to_csv(os.path.join(paths.data,'validation.csv')
                                ,index=False,header=False
                                ,columns=columns)
        # Test Data has only input features
        TestData = pd.read_csv(os.path.join(paths.storage_path,'dataset_ts_ventas_train.csv'), header=None)
        TestData.to_csv(os.path.join(paths.data,'test.csv'),header= False)
        return TrainData, ValidationData, TestData
    
