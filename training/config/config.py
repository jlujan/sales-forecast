import logging
import os

logging.basicConfig(filename='process.log',
                    level=logging.DEBUG,
                    format='%(asctime)s-%(levelname)s:%(message)s')


class Paths:
    """ Path Class create the neccesary  directories to handler training process
    """
    storage_path = os.environ.get('training',
                                  os.path.join(os.getcwd(), 'data'))

    output_to = os.path.join(storage_path)
    images    = os.path.join(output_to, 'images')
    data      = os.path.join(output_to, 'dataset')
    models    = os.path.join(output_to, 'models')

    for path in [images, data, models]:
        os.makedirs(path, exist_ok=True)