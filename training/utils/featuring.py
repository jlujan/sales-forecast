import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
from statsmodels.tsa.seasonal import MSTL

class data:

    def add_features(df):
        '''
        Add dynamic features to training dataset corresponding to:
        - year: numeric 
        - month: numeric 
        - dayofweek: numeric
        - workingdat: categorical-binary
        - season: categorical 1: sprint, 2:summer, 3: fall, 4: winter

        Parameters
        ----------
        train_ds: pd.DataFrame
        '''
        col_order = ['monto_total', 'precio_ref', 'unidades_total',	'year',	'month', 'day',	'dayofweek','workingday','season']

        df['year']       = df.index.year     
        df['month']      = df.index.month  
        df['day']        = df.index.day
        df['dayofweek']  = df.index.dayofweek
        df['workingday'] = np.where( df['dayofweek'] >= 5, 1, 0)
        df['season']     = df['month']%12 // 3 + 1
        df = df[col_order]
        return df
    
    def run_MSTL(paths,
                df, 
                target: str,
                period: list,
                iterator: int,
                kwargs: dict):
        '''
        Execute an MSTL analysis to train dataset. MSTL stands for Multiple Seasonal-Trend decomposition using Loess. 
        It is a method to decompose a time series into a trend component, multiple seasonal components, and a residual
        component. MSTL assumes that the time series can be expressed as an additive decomposition where each seasonal 
        component represents a different seasonality (e.g, daily, weekly, yearly, etc.). MSTL builds on STL to iteratively
        extract each of the seasonal components. Function Features:
        - year: numeric 
        - month: numeric
        - dayofweek: numeric
        - workingdat: categorical-binary
        - holiday: categorical-binary

        Parameters
        ----------
        train_ds: pd.DataFrame
        '''
        data = df[target]
        model = MSTL(data, periods=period, iterate= iterator, stl_kwargs=kwargs)
        res = model.fit()
        mstl_figure = res.plot()
        mstl_figure.savefig(os.path.join(paths.images,'MSTL_Analyse.png'))
        mstl_figure.show()
        return res

    def mutilV_analyser(df):
        """Plot pairwise feature relationships in train dataset
         Parameters
        ----------
        train_ds: pd.DataFrame
        """
        features_position_index = 0
        header = list(df.columns.values)
        # Features Pairplot
        sns.pairplot(df[header[features_position_index:]])
        plt.show()



        

