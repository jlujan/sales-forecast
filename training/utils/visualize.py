
import matplotlib.pyplot as plt
import seaborn as sns
import logging
import pandas as pd

sns.set_context('talk')
sns.set_style("darkgrid")

def plot_timeline(dataFrame, 
                target: str,
                max_subplots: int=3):
    '''
    Plot multiple windows of time-series train_ds.

    Parameters
    ----------
    train_ds: pd.DataFrame
    target: column name to plot
    '''
    try:
        value = dataFrame[target]
        plt.figure(figsize=(12, 8))
        plt.plot(dataFrame.index,value)
        plt.title("Dataset Monto_total vs Date")
        plt.xlabel("Date")
        plt.ylabel("Sales")
        plt.show()

    except:
        logging.error("Error Plotting Time-Series train_ds")
    
#def plot_season_behavior(df):

#   months_in_order = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
   
def covid_month_show(df,
                    month: str):
    """
    Compare multiple years sales for an specific month.

    Parameters
    ----------
    df: pd.DataFrame
    month: str -> month data to plot
    """
    try:
        years     = ['2017','2018','2019','2020','2021']
        month_key = [year +'-' + month for year in years]
        plt.plot(df[month_key[0]]['monto_total'],label='2017')
        plt.plot(df[month_key[1]]['monto_total'],label='2018')
        plt.plot(df[month_key[2]]['monto_total'],label='2019')
        plt.plot(df[month_key[3]]['monto_total'],label='2020')
        plt.plot(df[month_key[4]]['monto_total'],label='2021')
        plt.xticks(fontsize=14, rotation=45)
        plt.xlabel('Date')
        plt.ylabel('Sales')
        plt.title('Sales March by Year')
        plt.legend(loc=3, prop={'size': 8})
        plt.grid(True)
        plt.show()
    except:
        logging.error("Error Ploting Covid Monthly Comparison by year")

